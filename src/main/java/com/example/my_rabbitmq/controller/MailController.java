package com.example.my_rabbitmq.controller;

import com.example.my_rabbitmq.entities.Mail;
import com.example.my_rabbitmq.service.MailProducer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MailController {
    private final MailProducer mailProducer;
    public MailController(MailProducer mailProducer)
    {
        this.mailProducer=mailProducer;
    }
    @GetMapping("/send")
    public ResponseEntity<?> send(){
        Mail mail=Mail.builder()
                .to("bpokharel077@gmail.com")
                .subject("A test mail form RabbitMQ.")
                .body("This is a test mail sent during implementing RabbitMQ.")
                .build();
        for (int i=0;i<10;i++){
            mail.setId(i);
            mailProducer.produceMessage(mail);
        }
        return ResponseEntity.ok("Mail is sending to all the receiver.");
    }

}
