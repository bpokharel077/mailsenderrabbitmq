package com.example.my_rabbitmq.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Mail implements Serializable {
    private  int id;
    private String to;
    private String subject;
    private String body;
    private String template;
    private List<Object> attachments;
    private Map<String, Object> models;
}
