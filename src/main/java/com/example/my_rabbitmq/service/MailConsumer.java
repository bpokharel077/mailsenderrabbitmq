package com.example.my_rabbitmq.service;

import com.example.my_rabbitmq.entities.Mail;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class MailConsumer {
    @RabbitListener(queues = "mail.queue")
    public  void consumeMail(Mail mail){
        System.out.println(mail);
    }
}
