package com.example.my_rabbitmq.service;

import com.example.my_rabbitmq.entities.Mail;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
public class MailProducer {
    private static final String EXCHANGE_NAME="common.exchange";
    private static final String ROUTING_KEY="mail.queue.key";

    private final RabbitTemplate rabbitTemplate;

    public MailProducer(RabbitTemplate rabbitTemplate){
        this.rabbitTemplate = rabbitTemplate;

    }
    public void produceMessage(Mail mail)
    {
        rabbitTemplate.convertAndSend(EXCHANGE_NAME,ROUTING_KEY,mail);
        System.out.println("mail -" +mail.getId()+" has been sent to queue.");
    }
}
